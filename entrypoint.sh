#!/bin/bash

git -C fetchLandsatSentinelFromGoogleCloud/ pull

PYTHONPATH=$PYTHONPATH:./fetchLandsatSentinelFromGoogleCloud:./sen2mosaic DAGSTER_HOME=/app/dagster_home dagit -h 0.0.0.0 -f satellite_first_stage_pipeline.py
# jupyter notebook --notebook-dir=/app/notebooks --ip='*' --port=8888 --no-browser --allow-root

/bin/bash
# python3 main.py