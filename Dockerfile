# FROM python:3.9
# FROM osgeo/gdal:latest
FROM continuumio/anaconda3

ENV APP_HOME /app
WORKDIR $APP_HOME
RUN echo $APP_HOME

RUN git clone https://github.com/cwinkelmann/fetchLandsatSentinelFromGoogleCloud
RUN pip install -r fetchLandsatSentinelFromGoogleCloud/requirements/runtime.txt

RUN git clone https://github.com/smfm-project/sen2mosaic.git

# psutil
RUN conda config --add channels conda-forge
RUN conda install -y gdal #scipy pyshp opencv
RUN conda install -y scipy #pyshp opencv
RUN conda install -y pyshp
#RUN conda install -y scikit-image pandas


#RUN apt-get update
#RUN apt-get install gcc -y
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
#RUN pip install fiona
RUN apt-get update
RUN apt-get install -y nano

ENV PYTHONPATH=$PYTHONPATH:$APP_HOME/sen2mosaic:$APP_HOME/fetchLandsatSentinelFromGoogleCloud


