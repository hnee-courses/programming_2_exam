### Recommended setup using docker
docker is recommended, because there are many dependencies and some installations outside of a virtual env are required.
This assumes you have docker installed. If not do as described here: https://docs.docker.com/get-docker/

TODO get the paths right and explain the amount of data which is transported.

```shell
# build the container
docker build --tag satellite_data_processing:latest .
# run the container
## linux
docker run -it -v /media/christian/2TB/tmp:/persistent_tmp -v /media/christian/2TB/HNEE/GIS/fels:/gis/ -p 3000:3000 satellite_data_processing:latest /bin/bash

## windows
# docker run -it -e AOI_PATH=./area_of_interest/storm_damage_eberswalde_forest_campus.geojson -v I:/HNEE/GIS/tmp/:/persistent_tmp/ -v I:/HNEE/GIS/fels:/gis/ satellite_data_processing:latest /bin/bash
docker run -it -v I:/HNEE/GIS/fels/:/persistent_tmp/ -p 3000:3000 satellite_data_processing:latest /bin/bash
```
