from pathlib import Path

import yaml
import io
import click


def get_mosaic_pipeline_config(base_path, aoi_path, crs, resolution = 60):
    """
    setup the config for the mosaicing pipeline. This includes information for the downloading the sentinel 2 index,
    processing it, downloading all needed tiles and then mosaicing them to cloudfree stitches of the whole AOI

    :param base_path:
    :param aoi_path:
    :return:
    """

    return {
        "SENTINEL2_METADATA_URL": "http://storage.googleapis.com/gcp-public-data-sentinel-2/L2/index.csv.gz",
        "zipped_index_path": f"{base_path}/fels/",
        "zipped_index_filename": "index_Sentinel.csv.gz",
        "file_input_folder": f"{base_path}/fels/download3",
        "temporal_mosaic_path": f"{base_path}/mosaic/",
        "index_input_folder": f"{base_path}/fels/index_Sentinel_parquet",
        "index_path": f"{base_path}/fels/_dev_fels/index_Sentinel_parquet",
        "mosaic_base_path": f"{base_path}/mosaic/",
        "reduced_index_path": f"{base_path}/fels/index_reduced_Sentinel_parquet",
        "aoi_path": f"{aoi_path}",
        "max_index_age": 7 * 24 * 3600,
        "crs": crs,
        "max_cloud_cover": 70.0,
        "resolution": resolution,
        "bands": ["B04", "B8A"],

        "band": "NDVI",
        "start_date": "2021-06-01",
        "end_date": "2021-09-30",
        "animation_path": f"{base_path}/mosaic/",
    }


def load_yaml_from_disk(yaml_path: str):
    """

    :param yaml_path:
    :return:
    """
    import yaml

    with open(yaml_path, "r") as stream:
        try:
            loaded_config = yaml.safe_load(stream)
            return loaded_config
        except yaml.YAMLError as exc:
            print(exc)


@click.command()
@click.option(
    "--data_base_path",
    prompt="storage path",
    help="base path for data. Should have more than 20GB free space",
)
@click.option(
    "--aoi_path",
    default="./configuration/area_of_interest/storm_damage_eberswalde_forest_campus.geojson",
    prompt="file path to AOI geojson",
    help="file path to geojson",
)
@click.option(
    "--crs",
    default="",
    prompt="EPSG Code of your AOI",
    help="If the projection does not match the aoi problems finding the wgrs tiles will occur. Look at https://epsg.io/ to find a code. The default geojsons have the correct epsg codes.",
)
@click.option(
    "--yaml_path",
    default="./configuration/pipeline_configuration_demo.yml",
    prompt="output yml file of the configuration",
)
def build_config_yml(data_base_path, aoi_path, crs, yaml_path):
    """
    helper script to build necessary configuration files for the processing pipeline.

    will generate a yml file with contains all information needed

    """
    return _build_config_yml(data_base_path, aoi_path, crs, yaml_path)


def _build_config_yml(data_base_path, aoi_path, crs, yaml_path):
    resolution = None
    if crs == "":
        if aoi_path == "./configuration/area_of_interest/antarctica_ice_coast.geojson":
            crs = 32710
        if aoi_path == "./configuration/area_of_interest/McMurdo.geojson":
            crs = 32758
        if (
            aoi_path
            == "./configuration/area_of_interest/storm_damage_eberswalde_forest_campus.geojson"
        ):
            crs = 32633
        if aoi_path == "./configuration/area_of_interest/schorfheide.geojson":
            crs = 32633
        if aoi_path == "./configuration/area_of_interest/CumbreVieja.geojson":
            crs = 5634
        if aoi_path == "./configuration/area_of_interest/tonga.geojson":
            crs = 5887
            resolution = 10
    else:
        crs = int(crs)

    data_base_path = str(
        Path(data_base_path)
    )  # just to make sure if there is a '/' too much
    if resolution is None:
        mosaic_pipeline_config = get_mosaic_pipeline_config(data_base_path, aoi_path, crs)
    else:
        mosaic_pipeline_config = get_mosaic_pipeline_config(data_base_path, aoi_path, crs, resolution=resolution)
    with io.open(yaml_path, "w", encoding="utf8") as outfile:
        yaml.dump(
            mosaic_pipeline_config,
            outfile,
            default_flow_style=False,
            allow_unicode=True,
        )
        print(
            f"wrote config to this path: {Path(__file__).parent.parent.resolve().joinpath(yaml_path)}"
        )

    # load the config yaml from disk
    loaded_config = load_yaml_from_disk(yaml_path)
    return loaded_config


if __name__ == "__main__":
    # base_path = "/media/christian/2TB/tmp"
    #
    # current_file_path = Path(__file__).parent.resolve()
    # yml_path = current_file_path.joinpath("../configuration/pipeline_configuration_examdemo.yml").resolve()
    # aoi_path = current_file_path.joinpath(
    #     "../configuration/area_of_interest/storm_damage_eberswalde_forest_campus.geojson").resolve()
    #
    # build_config_yml(data_base_path=base_path, aoi_path=aoi_path,
    #                  yaml_path=yml_path, func=get_mosaic_pipeline_config)
    #
    # yml_path = current_file_path.joinpath("../configuration/timeseries_pipeline_examdemo.yml").resolve()

    # build_config_yml(data_base_path=base_path, aoi_path=aoi_path,
    #                  yaml_path=yml_path, func=get_timeseries_pipeline_config)

    build_config_yml()
