import dateutil
import pandas as pd
import os
from pathlib import Path
import calendar

from dagster import get_dagster_logger
from pyspark.sql import SparkSession
from loguru import logger


def trigger_mosaicing(
    wgrs_tile,
    year,
    month,
    crs,
    resolution,
    bands,
    extent: dict,
    input_folder,
    output_path: Path,
):
    """
    use the sen2mosaic cli to stitch together all sentinel 2 imagery from a month in the input_folder

    See this documentation: https://sen2mosaic.readthedocs.io/en/latest/command_line.html#processing-to-a-mosaic

    :param wgrs_tile:
    :param year:
    :param month:
    :param crs:
    :param resolution:
    :param extent:
    :param input_folder:
    :return:
    """
    from dagster import get_dagster_logger

    """
    On the northern hemisphere the values are positive
    On the southern hemisphere the values are negative

    """
    xmin = round(extent.get("xmin"))
    xmax = round(extent.get("xmax"))
    ymin = round(extent.get("ymin"))
    ymax = round(extent.get("ymax"))

    from datetime import date

    date_object = date(year, month, 1)
    year_month = date_object.strftime("%Y%m")
    start_date = f"{year_month}01"
    res = calendar.monthrange(date_object.year, date_object.month)[1]
    end_date = f"{year_month}{res}"

    # if not output_path.exists(): ## TODO implement a check to validate files are present

    Path(output_path).mkdir(parents=True, exist_ok=True)
    current_file_path = Path(__file__).parent.resolve()
    FeLS_path = current_file_path.joinpath(
        "../fetchLandsatSentinelFromGoogleCloud"
    ).resolve()
    sen2mosaic_path = current_file_path.joinpath("../sen2mosaic").resolve()

    formatted_input_files = " ".join(input_folder)
    bands = ",".join(bands)
    bands = f"'{bands}'"
    mosaic_command = f"PYTHONPATH=$PYTHONPATH:{sen2mosaic_path} python3 sen2mosaic/cli/mosaic.py -p 1 -e {crs} -st {start_date} -en {end_date} -te {xmin} {ymin} {xmax} {ymax} -res {resolution} -bands {bands} -o {output_path} {formatted_input_files}"

    trigger_mosaicing_logger = get_dagster_logger()
    trigger_mosaicing_logger.info(f"mosaic command: {mosaic_command}")
    logger.info(f"mosaic command: {mosaic_command}")
    return_code = os.system(mosaic_command)
    if return_code != 0:
        raise Exception(
            f"got: return_code:{return_code}. command did not succeed, check if it works. {mosaic_command}"
        )
    else:
        logger.info(f"mosaicing was successful. Data was written to: {output_path}")
    return output_path


def prepare_filelist_for_mosaicing(
    wgrs_tile, year, month, cc_limit, file_input_folder, index_input_folder
):
    """
    From a folder of many SAFE files take all which are in the tile and match year and month
    :param wgrs_tile:
    :param year:
    :param file_input_folder:
    :param index_input_folder:
    :return:
    """
    import glob

    # get a list of all downloaded imges
    images_full_path = glob.glob(file_input_folder + "/S2*.SAFE")
    images = [x.split("/")[-1] for x in images_full_path]

    images = pd.DataFrame({"images": images, "images_full_path": images_full_path})
    images["aquisition_time"] = images.apply(
        lambda x: dateutil.parser.isoparse(x["images"].split("_")[2]), axis=1
    )
    images["WGRS_tile"] = images.apply(lambda x: x["images"].split("_")[5], axis=1)
    images["product_id"] = images.apply(lambda x: x["images"].split(".")[0], axis=1)

    df_downloaded_tiles = images

    spark = (
        SparkSession.builder.master("local[4]").appName("parquet_example").getOrCreate()
    )

    parqDF = spark.read.parquet(index_input_folder)
    parqDF.createOrReplaceTempView("ParquetTable")
    trigger_mosaicing_logger = get_dagster_logger()
    query = f"select * from ParquetTable where MGRS_TILE IN ('{wgrs_tile}') "
    trigger_mosaicing_logger.info(f"run query to find relevant tiles: '{query}'")
    logger.info(f"run query to find relevant tiles: '{query}'")
    df_index = spark.sql(query).toPandas()
    spark.stop()
    # join the info from
    df_joined_tiles = df_downloaded_tiles.merge(
        df_index, how="inner", left_on="product_id", right_on="PRODUCT_ID"
    )

    df_filtered = df_joined_tiles[
        (df_joined_tiles["SENSING_TIME"].dt.month == month)
        & (df_joined_tiles["SENSING_TIME"].dt.year == year)
        & (df_joined_tiles["CLOUD_COVER"] < cc_limit)
    ]

    return df_filtered
