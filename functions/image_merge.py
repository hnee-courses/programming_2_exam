import json
import imageio as iio
from pathlib import Path

import sys
from glob import glob
from osgeo import ogr, gdal
from osgeo import gdalconst
import subprocess

import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from matplotlib.colors import ListedColormap, BoundaryNorm
import numpy as np

import seaborn as sns

import geopandas as gpd
import pycrs
import fiona
from fiona.crs import from_epsg
from shapely.geometry import box
from shapely.geometry import Point
import shapely.geometry as geoms

import rasterio as rio
from rasterio.plot import show
import rasterio.warp
import rasterio.shutil
from rasterio.merge import merge
from rasterio.warp import reproject

# from rasterio.warp import calculate_default_transform, reproject, Resampling
# from rasterio.plot import plotting_extent
# from rasterio.plot import show_hist
# from rasterio.mask import mask
# from rasterio.merge import merge
# from rasterio import Affine, MemoryFile
# from rasterio.enums import Resampling
# from rasterio import plot
#
# import rasterstats as rs
# import georasters as gr
# from rastertodataframe import raster_to_dataframe
#
# import earthpy.spatial as es
# import earthpy.plot as ep
# import earthpy as et
#
# from sklearn.model_selection import train_test_split
# from sklearn.linear_model import LinearRegression
# from sklearn import metrics
# from sklearn.model_selection import cross_val_score
# from sklearn.model_selection import KFold
# from sklearn.model_selection import cross_val_predict
# from sklearn.model_selection import cross_validate


def temporal_merge(images_paths: list):
    """

    :param images_paths:
    :return:
    """
    pass


def getFeatures(gdf):
    """Function to parse features from GeoDataFrame in such a manner that rasterio wants them"""
    import json

    return [json.loads(gdf.to_json())["features"][0]["geometry"]]


def spatial_merge(
    images_base: list, band_mosaics: list, resolution, mosaic_base_path: Path
):
    """
    merge all images in the list

    if you have images which don't overlapp you will potentially create very big images.
    !CAUTION: if you are passing a float images [-1...1] like a ndvi it will not work because of a bug in gdal.
    :param images_base:
    :param band_mosaics:
    :param resolution:
    :param mosaic_base_path:
    :return:
    """

    output_paths = []
    for band_mosaic in band_mosaics:
        # List for the source files
        src_files_to_mosaic = []

        # Iterate over raster files and add them to source -list in 'read mode'
        for fp in images_base:
            band_mosaic_file_name = f"mosaic_R{resolution}m_{band_mosaic}.tif"
            filename = Path(fp).joinpath(band_mosaic_file_name)
            src = rasterio.open(str(filename))
            src_files_to_mosaic.append(src)

        # Merge function returns a single mosaic array and the transformation info
        mosaic, out_trans = merge(src_files_to_mosaic)

        # Plot the result
        # show(mosaic, cmap='terrain')

        # Copy the metadata
        out_meta = src.meta.copy()

        # Update the metadata
        out_meta.update(
            {
                "driver": "GTiff",
                "height": mosaic.shape[1],
                "width": mosaic.shape[2],
                "transform": out_trans,
                "crs": src.crs,
            }
        )
        out_fp = str(mosaic_base_path.joinpath(band_mosaic_file_name))
        output_paths.append(out_fp)
        with rasterio.open(out_fp, "w", **out_meta) as dest:
            dest.write(mosaic)

    return output_paths


def spatial_merge_vrt(clipped_images: list, out_fp: str):
    """
    build a virtual raster (vrt) image
    :param clipped_images:
    :param out_fp:
    :return:
    """
    # List for the source files
    src_files_to_mosaic = []

    # Iterate over raster files and add them to source -list in 'read mode'
    for fp in clipped_images:
        src = rasterio.open(fp)
        src_files_to_mosaic.append(src)

    # Merge function returns a single mosaic array and the transformation info
    mosaic, out_trans = merge(src_files_to_mosaic)

    # Plot the result
    show(mosaic, cmap="terrain")


def clip_image(input_path, aoi_path, resolution, output_path):
    """
    clip the part of the raster which is not in the area of interest

    :param input_path:
    :param aoi_path:
    :param output_path:
    :return:
    """
    import os
    import rasterio
    from rasterio.plot import show
    from rasterio.plot import show_hist
    from rasterio.mask import mask
    from shapely.geometry import box
    import geopandas as gpd
    from fiona.crs import from_epsg
    import pycrs

    # Read the data
    data = rasterio.open(input_path)

    # Visualize the NIR band
    # show((data, 1), cmap='RdYlGn')

    # Project the Polygon into same CRS as the grid
    geo = gpd.GeoDataFrame.from_file(aoi_path)
    print(f"before: {geo.crs}")
    geo = geo.to_crs(crs=data.crs.data)

    # Print crs
    print(f"after: {geo.crs}")

    coords = getFeatures(geo)
    # print(coords)

    # Clip the raster with Polygon
    out_img, out_transform = mask(dataset=data, shapes=coords, crop=True)
    out_meta = data.meta.copy()
    # print(out_meta)

    out_meta.update(
        {
            "driver": "GTiff",
            "height": out_img.shape[1],
            "width": out_img.shape[2],
            "transform": out_transform,
            "crs": data.crs,
        }
    )

    with rasterio.open(output_path, "w", **out_meta) as dest:
        dest.write(out_img)


if __name__ == "__main__":
    base_paths = [
        # Path('/media/christian/2TB/HNEE/GIS/tmp/mosaic/33UUU/202103/60/'),
        Path("/media/christian/2TB/HNEE/GIS/tmp/mosaic/33UVV/202103/60/"),
        Path("/media/christian/2TB/HNEE/GIS/tmp/mosaic/33UUV/202103/60/"),
        # Path('/media/christian/2TB/HNEE/GIS/tmp/mosaic/32UQD/202103/60/'),
        # Path('/media/christian/2TB/HNEE/GIS/tmp/mosaic/33UVU/202103/60/'),
        # Path('/media/christian/2TB/HNEE/GIS/tmp/mosaic/32UQE/202103/60/')
    ]

    input_image = "mosaic_R60m_NDVI.tif"
    output_image_sliced = "mosaic_R60m_NDVI_sliced.tif"

    # Write the mosaic raster to disk
    # out_fp = "/media/christian/2TB/HNEE/GIS/tmp/mosaic/Schorfheide_Mosaic_local.tif"

    # clipped_images = []
    # for base_path in base_paths:
    #     # Input raster
    #     #fp = base_path.joinpath("mosaic_R60m_NDVI.tif")
    #     fp = base_path
    #     aoi_path = "/home/christian/work/hnee/programming_2_exam/area_of_interest/schorfheide.geojson"
    #     out_tif = base_path.joinpath(output_image_sliced)
    #     clip_image(input_path=fp, aoi_path=aoi_path, output_path=out_tif)
    #     clipped = rasterio.open(out_tif)
    #     clipped_images.append(out_tif)

    test_images = [
        base_path.joinpath("mosaic_R60m_NDVI.tif") for base_path in base_paths
    ]

    test_images = [
        Path(
            "/media/christian/2TB/HNEE/GIS/fels/_dev_fels/2021/S2A_MSIL2A_20200102T102421_N0213_R065_T32UQD_20200102T115419.SAFE/GRANULE/L2A_T32UQD_A023657_20200102T102422/IMG_DATA/R60m/T32UQD_20200102T102421_B02_60m.jp2"
        ),
        Path(
            "/media/christian/2TB/HNEE/GIS/fels/_dev_fels/2021/S2A_MSIL2A_20200102T102421_N0213_R065_T33UUU_20200102T115419.SAFE/GRANULE/L2A_T33UUU_A023657_20200102T102422/IMG_DATA/R60m/T33UUU_20200102T102421_B02_60m.jp2"
        ),
    ]

    spatial_merge(clipped_images=test_images, out_fp=out_fp)
