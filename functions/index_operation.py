from loguru import logger


def build_custom_index(sentinel2_metadata_file, reduced_index_path, scenes):
    """
    from the scenes we are building an index which contains only these tiles

    :param context:
    :param scenes:
    :return:
    """
    from pyspark.sql import SparkSession

    spark = (
        SparkSession.builder.master("local[4]").appName(f"reduced_index").getOrCreate()
    )

    parqDF = spark.read.parquet(sentinel2_metadata_file)
    parqDF.createOrReplaceTempView(f"ParquetTable")
    scenes = [f"'{scene}'" for scene in scenes]
    scenes_str = ", ".join(scenes)
    query = f"select * from ParquetTable where MGRS_TILE IN ({scenes_str})"
    logger.info(
        f"run the query: {query} and save the result as a new index file at {reduced_index_path} to speed up read access."
    )
    tilelist_df = spark.sql(query)
    ## see https://gitlab.com/hnee-courses/programming_2_exam/-/issues/9
    tilelist_df.repartition(1).write.mode("overwrite").parquet(reduced_index_path)
    spark.stop()
    logger.info(f"New index file at {reduced_index_path} is ready")

    return tilelist_df
