##
# calculate the extent of the image which will be necessary for the mosaic
#
##
from pathlib import Path
import pandas as pd


def find_extent(urls_df: pd.DataFrame, crs):
    """
    calculate the biggest extent of many images
    :param urls_df:
    :return:
    """
    # https://pyproj4.github.io/pyproj/stable/api/proj.html
    from pyproj import Proj

    p = Proj(f"epsg:{crs}", preserve_units=False)

    xmin, ymin = p(longitude=urls_df["WEST_LON"], latitude=urls_df["SOUTH_LAT"])
    xmax, ymax = p(longitude=urls_df["EAST_LON"], latitude=urls_df["NORTH_LAT"])

    xmin = min(xmin)
    ymin = min(ymin)

    xmax = max(xmax)
    ymax = max(ymax)

    extent = {"xmin": xmin, "xmax": xmax, "ymin": ymin, "ymax": ymax}

    return extent
