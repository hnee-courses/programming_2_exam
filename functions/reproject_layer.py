import numpy as np
import rasterio
from rasterio import Affine as A
from rasterio.warp import reproject, Resampling


def reproject_mosaic(
    input_path="/media/christian/2TB/HNEE/GIS/tmp/mosaic/33UUV/202110/60/mosaic_R60m_NDVI.tif",
    output_path="/tmp/zoomed-out.tif",
    target_crs=None,
):

    with rasterio.open(input_path) as src:
        src_transform = src.transform
        src.crs
        # The destination transform is the product of the
        # source transform, a translation down and to the right, and
        # a scaling.
        dst_transform = src_transform
        data = src.read()

        kwargs = src.meta
        kwargs["transform"] = dst_transform

        with rasterio.open(output_path, "w", **kwargs) as dst:
            # in case we have more bands
            for i, band in enumerate(data, 1):
                dest = np.zeros_like(band)

                reproject(
                    band,
                    dest,
                    src_transform=src_transform,
                    src_crs=src.crs,
                    dst_transform=dst_transform,
                    dst_crs=src.crs,
                    # resampling=Resampling.nearest
                )

                dst.write(dest, indexes=i)


if __name__ == "__main__":

    with rasterio.open(
        "/media/christian/2TB/HNEE/GIS/tmp/mosaic/32UQD/202110/60/mosaic_R60m_NDVI.tif"
    ) as src:
        mycrs = src.crs

    reproject_mosaic(target_crs=mycrs)
