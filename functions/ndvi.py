from pathlib import Path

import rasterio
import rasterio.plot
import matplotlib.pyplot as plt

oview = 1


def calc_ndvi(nir, red):
    """
    formula for normalized difference vegetation index

    :param nir:
    :param red:
    :return:
    Calculate NDVI from integer arrays
    """
    nir = nir.astype("f4")
    red = red.astype("f4")
    ndvi = (nir - red) / (nir + red)
    return ndvi


def calculate_and_save_ndvi(base_path: str, resolution, year, month) -> str:
    """
    wrapper to calculate the ndvi and save it to file

    :param resolution:
    :param base_path:
    :return:
    """
    base_path = Path(base_path)

    with rasterio.open(
        base_path.joinpath(f"mosaic_R{resolution}m_B04.tif_clipped.tif")
    ) as src:
        profile = src.profile
        red = src.read(
            1, out_shape=(1, int(src.height // oview), int(src.width // oview))
        )

    with rasterio.open(
        base_path.joinpath(f"mosaic_R{resolution}m_B8A.tif_clipped.tif")
    ) as src:
        nir = src.read(
            1, out_shape=(1, int(src.height // oview), int(src.width // oview))
        )

    ndvi = calc_ndvi(nir, red)

    plt.imshow(ndvi, cmap="RdYlGn")

    plt.colorbar()
    plt.clim(-1, 1)  # for better visualization (0,1) would be better
    plt.title(f"NDVI {year}-{month}, resolution: {resolution}m")
    plt.xlabel("Column #")
    plt.ylabel("Row #")

    # plt.show()
    output_file = str(base_path.joinpath(f"NDVI_R{resolution}m.png"))
    plt.savefig(output_file)
    plt.close()

    # save the NDVI as a GeoTiff
    aff = src.transform
    newaff = rasterio.Affine(aff.a * oview, aff.b, aff.c, aff.d, aff.e * oview, aff.f)
    profile.update(
        {
            "dtype": "float32",
            "height": ndvi.shape[0],
            "width": ndvi.shape[1],
            "transform": newaff,
        }
    )

    with rasterio.open(
        base_path.joinpath(f"mosaic_R{resolution}m_NDVI.tif"), "w", **profile
    ) as dst:
        dst.write_band(1, ndvi)

    return str(base_path.joinpath(f"mosaic_R{resolution}m_NDVI.tif"))


if __name__ == "__main__":
    calculate_and_save_ndvi(
        base_path="/media/christian/2TB/HNEE/GIS/tmp/mosaic/33UUU/202107/60",
        resolution=60,
        year=2021,
        month=4,
    )
