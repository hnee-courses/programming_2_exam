# Programming Assignment for the course Programming II

A pipeline to calculate changes in NDVI on Sentinel-2 satellite images. 

The process is split into three main parts:
- obtaining Level 2 Sentinel-2 Data
- generating cloud free mosaics
- processing the NDVI for an area of interest defined by a geojson
- creating a gif for the images


## Getting started

### install dependencies
Given you are reading this README.md you are already in the root directory of the project. You should have a running python environment using anaconda, pip and git. Using PyCharm would make some things easier, so that is recommened. This guide was written using ubuntu20.04. Because of many optimizations during index building it will not work on windows.

The steps are:
- environment setup
- Setup of forked libraries: "Sen2Mosaic" and "fetchLandsatSentinelFromGoogleCloud"
- installation of all project dependencies.

#### environment setup
Setup an environment called "programming_2_exam". If it is already active existing deactivate and delete it.
```shell
conda deactivate

conda env remove -n programming_2_exam 
```



Now we create the environment using conda. Then activate it.
```shell
conda create --name programming_2_exam -y python=3.8
conda activate programming_2_exam
```

Optionally add the interpreter to PyCharm for later use. Use the add interpreter dialogue to do so.
![img.png](doc/python_interpreter.png)


#### get Fels Fork and sen2mosaic
```shell
git clone https://github.com/cwinkelmann/fetchLandsatSentinelFromGoogleCloud
git clone https://github.com/cwinkelmann/sen2mosaic.git
```

Optionally add both directories to the PYTHON_PATH of PyCharm.

![img.png](doc/img.png)

Go ``` File->Settings->Project->Project Structure ``` and add the both directories as sources
![img_1.png](doc/img_1.png)

#### install dependencies

```shell
pip install -r fetchLandsatSentinelFromGoogleCloud/requirements/runtime.txt
conda config --add channels conda-forge
conda install -y gdal scipy pyshp 
pip install -r requirements.txt
```

**If gdal installation fails** Depending on your distribution you might need to install dependencies regarding gdal. See [gdal.org](https://gdal.org/download.html#binaries) for help. After this repeat the step above.
```shell
sudo apt install libpq-dev gdal-bin libgdal-dev
```

Test if everything is fine. This will test some functions which are needed in the code.
```shell
PYTHONPATH=$PYTHONPATH:./fetchLandsatSentinelFromGoogleCloud:./sen2mosaic pytest tests
```

Next is a smoke test, which will run the whole pipeline. This will download about 2GB and occupies about 15GB of Diskspace in /tmp. If you want to change the base directory you can do it in smoketest/test_pipelines_smoke.py. The test will take around 10 minutes depending on your download/CPU and Diskspeed
```python
_build_config_yml(data_base_path=current_file_path.joinpath("/tmp/")
```

Run the smoke test here. This is the smallest possible execution of the whole pipeline. This will download the index and some tiles.:
```shell 
PYTHONPATH=$PYTHONPATH:./fetchLandsatSentinelFromGoogleCloud:./sen2mosaic pytest smoketest
```
If the tests where all ok you are good do go. There should be three interesting graphs and a gif animation in the folder /tmp/mosaic by now.

If not contact the author.


### Usage
After the tests the software can be used how it is supposed to. You will trigger processing from dagster GUI. The scheduler will then decide what to run first.

#### Generate a configuration
Before you can start you will need to generate a configuration. Basically these are two paths.
1. the data base path. Everything from the index to the raw and processed satellite images are saved there. This will easily be about 100GB
2. the path to a geojson, which depicts the area of interest you are interested in. The repository comes with some samples in the folder "./configuration/area_of_interest/"


```shell
# get a basic configuration for the first stage. TODO change the data_base_path to a folder on your disk with some free space. Use --data_base_path="/tmp" when unsure
python3 functions/get_pipeline_config.py --aoi_path="./configuration/area_of_interest/storm_damage_eberswalde_forest_campus.geojson" --crs=32633 --data_base_path="/media/christian/2TB/tmp"  

# for the whole biosphere schorfheide Chorin
python3 functions/get_pipeline_config.py --aoi_path="./configuration/area_of_interest/schorfheide.geojson" --crs=32633 --data_base_path="/media/christian/2TB/tmp"  

# If you have a bit more time, fast internet and more space on your disk try something more fun like antarctica. But don't forget in July there is complete darkness
# get a basic configuration for the first stage.
python3 functions/get_pipeline_config.py --aoi_path="./configuration/area_of_interest/McMurdo.geojson" --data_base_path="/media/christian/2TB/tmp"
```

#### Dagster scheduling
If you have many more than 4 CPU cores, at least 16GB of RAM and a fast internet connection and around 100GB of storage there you can have some fun. This assumes fetchLandsatSentinelFromGoogleCloud and sen2mosaic are there in these relative location.
```shell
# fast result
YML_CONFIG_PATH=<YOUR_ABSOLUTE_PATH_TO_THE_CONFIG>/pipeline_configuration_examdemo.yml PYTHONPATH=$PYTHONPATH:./fetchLandsatSentinelFromGoogleCloud:./sen2mosaic DAGSTER_HOME=$PWD/configuration/dagster_home dagster-daemon run
```

run the DAG here
```bash
YML_CONFIG_PATH=<YOUR_ABSOLUTE_PATH_TO_THE_CONFIG>/pipeline_configuration_examdemo.yml PYTHONPATH=$PYTHONPATH:./fetchLandsatSentinelFromGoogleCloud:./sen2mosaic DAGSTER_HOME=$PWD/configuration/dagster_home dagit -f satellite_first_stage_pipeline.py
```

Generate the Animation
```bash
YML_CONFIG_PATH=<YOUR_ABSOLUTE_PATH_TO_THE_CONFIG>/<THE SECOND STAGE CONFIG>.yml PYTHONPATH=$PYTHONPATH:./fetchLandsatSentinelFromGoogleCloud:./sen2mosaic DAGSTER_HOME=$PWD/configuration/dagster_home dagit -f satellite_second_stage_pipeline.py
```

If your just generated config is in "/home/christian/work/hnee/programming_2_exam/functions/configuration/pipeline_configuration_demo.yml" then you trigger the dagster daemon like this:
please use three open terminals
```shell
# terminal 1
YML_CONFIG_PATH=/home/christian/work/hnee/programming_2_exam/configuration/pipeline_configuration_demo.yml PYTHONPATH=$PYTHONPATH:./fetchLandsatSentinelFromGoogleCloud:./sen2mosaic DAGSTER_HOME=$PWD/configuration/dagster_home dagster-daemon run
# terminal 2
YML_CONFIG_PATH=/home/christian/work/hnee/programming_2_exam/configuration/pipeline_configuration_demo.yml PYTHONPATH=$PYTHONPATH:./fetchLandsatSentinelFromGoogleCloud:./sen2mosaic DAGSTER_HOME=$PWD/configuration/dagster_home dagit -f satellite_first_stage_pipeline.py

# terminal 3 ( check start_date and end_date in pipeline_configuration_demo first )
YML_CONFIG_PATH=/home/christian/work/hnee/programming_2_exam/configuration/pipeline_configuration_demo.yml PYTHONPATH=$PYTHONPATH:./fetchLandsatSentinelFromGoogleCloud:./sen2mosaic DAGSTER_HOME=$PWD/configuration/dagster_home dagit -f satellite_second_stage_pipeline.py

```
Open up the web frontend for the first pipeline: http://127.0.0.1:3000/ 
Launch a backfill (upper left) for the first pipeline to download all data necessary for the timerange. If you want a gif animation from 2021-06 to 2021-07 just select these. Check end_date and start_date in pipeline_configuration_demo.yml.
[Backfill creation](http://127.0.0.1:3000/workspace/satellite_imagery_repository@satellite_first_stage_pipeline.py/jobs/ndvi_gif_calculation/partitions)
![img.png](doc/DAG_1_backfill.png)
At the first run a lot of time will be consumed for downloading an processing the index. Have a look at gui. Try having a look at spark ui http://localhost:4040/jobs/ to see if it is running in the background.


and the second pipeline http://127.0.0.1:3001/
Create a single run via the [launchpad](http://127.0.0.1:3001/workspace/__repository__timeseries_analytics_job@satellite_second_stage_pipeline.py/jobs/timeseries_analytics_job/playground): You don't need to change anything, just click "Launch Run" in the bottom right corner.
![img.png](doc/dag_2_launchpad_.png)



#### Download satellite data for your area of interest

Downloading Sentinel 2 Data
```bash
# Docker version
docker run -it -e OUTPUT=/persistent_temp -e OUTPUT_CATALOGS=/gis/ -v /media/christian/2TB/tmp:/persistent_tmp -v /media/christian/2TB/HNEE/GIS/fels:/gis/ satellite_data_processing:latest python3 main.py

```
run the mosaicing manually
```bash
PYTHONPATH=$PYTHONPATH:./fetchLandsatSentinelFromGoogleCloud:./sen2mosaic python3 sen2mosaic/cli/mosaic.py -p 1 -e 32633 -st 20200401 -en 20200430 -te 397664 5790245 509750 5900058 -res 60 -bands "B02,B03,B8A" -o /media/christian/2TB/tmp/fels/download2/S2B_MSIL2A_20200426T101549_N0214_R065_T33UVU_20200426T141031.SAFE /media/christian/2TB/tmp/fels/download2/S2B_MSIL2A_20200410T100029_N0214_R122_T33UVU_20200410T134801.SAFE /media/christian/2TB/tmp/fels/download2/S2A_MSIL2A_20200418T101031_N0214_R022_T33UVU_20200418T133933.SAFE /media/christian/2TB/tmp/fels/download2/S2A_MSIL2A_20200405T100021_N0214_R122_T33UVU_20200405T115512.SAFE /media/christian/2TB/tmp/fels/download2/S2B_MSIL2A_20200403T100549_N0214_R022_T33UVU_20200403T135337.SAFE /media/christian/2TB/tmp/fels/download2/S2B_MSIL2A_20200423T100549_N0214_R022_T33UVU_20200423T144611.SAFE /media/christian/2TB/tmp/fels/download2/S2A_MSIL2A_20200408T101021_N0214_R022_T33UVU_20200408T161405.SAFE /media/christian/2TB/tmp/fels/download2/S2A_MSIL2A_20200428T101031_N0214_R022_T33UVU_20200428T115829.SAFE /media/christian/2TB/tmp/fels/download2/S2A_MSIL2A_20200411T102021_N0214_R065_T33UVU_20200411T143032.SAFE /media/christian/2TB/tmp/fels/download2/S2B_MSIL2A_20200420T100019_N0214_R122_T33UVU_20200420T130740.SAFE /media/christian/2TB/tmp/fels/download2/S2A_MSIL2A_20200425T100031_N0214_R122_T33UVU_20200425T125204.SAFE /media/christian/2TB/tmp/fels/download2/S2B_MSIL2A_20200416T101549_N0214_R065_T33UVU_20200416T135611.SAFE /media/christian/2TB/tmp/fels/download2/S2B_MSIL2A_20200413T100549_N0214_R022_T33UVU_20200413T132357.SAFE /media/christian/2TB/tmp/fels/download2/S2A_MSIL2A_20200421T102021_N0214_R065_T33UVU_20200421T120142.SAFE /media/christian/2TB/tmp/fels/download2/S2B_MSIL2A_20200406T101559_N0214_R065_T33UVU_20200406T134514.SAFE
```

### Docker
The docker implementation is not done. But laid out. Docker enables you to encapsulate everything from you operating system. So the only thing you need to install is Docker.
```bash
docker build -t dagster_imagery_pipeline .
```


### Trouble Shooting
#### sen2mosaic error 512
sen2mosaic return error 512 directly after calling. This is probably because of missing data. Execute the command by copy and pase it from the logs.
The other reason could be a problem with the PYTHONPATH