import json
import os
import unittest
from datetime import date
from pathlib import Path

from fels import run_fels
from functions.get_pipeline_config import _build_config_yml


class ConfigTest(unittest.TestCase):
    def test_build_config(self):
        current_file_path = Path(__file__).parent.resolve()
        yml_path = current_file_path.joinpath(
            "./data/pipeline_configuration_demo.yml"
        ).resolve()
        aoi_path = current_file_path.joinpath(
            "../SOME/RANDOM/FOLDER/storm_damage_eberswalde_forest_campus.geojson"
        ).resolve()
        loaded_config = _build_config_yml(
            data_base_path="./THIS/IS/ONLY/A/TESTING/PATH",
            aoi_path=aoi_path,
            yaml_path=yml_path,
            crs="",
        )
        print(loaded_config)
        self.assertTrue(len(loaded_config) > 0)


if __name__ == "__main__":
    unittest.main()
