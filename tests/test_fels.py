import json
import os
import unittest
from datetime import date
from pathlib import Path

from fels import run_fels
from fels.sentinel2 import filter_manifest_lines


class FeLSTest(unittest.TestCase):
    def test_reduce_urls(self):
        current_file_path = Path(__file__).parent.resolve()
        with open(
            current_file_path.joinpath("./data/manifest.safe"), "r"
        ) as manifest_file:
            manifest_lines = manifest_file.read().split()

        result = filter_manifest_lines(
            manifest_lines=manifest_lines, resolution=60, bands=["B02", "B03", "B8A"]
        )

        self.assertEquals(
            len(result), 7, "only resolutions of 60 should in there. And the 20m SLC. "
        )

    def test_reduce_urls_10m(self):
        """ test if the 20m resolution file is in the list """
        current_file_path = Path(__file__).parent.resolve()
        with open(
            current_file_path.joinpath("./data/manifest.safe"), "r"
        ) as manifest_file:
            manifest_lines = manifest_file.read().split()

        result = filter_manifest_lines(
            manifest_lines=manifest_lines, resolution=10, bands=["B02", "B03", "B8A"]
        )

        self.assertEquals(
            len(result), 6, "only resolutions of 10 should in there. And the 20m SLC. "
        )


if __name__ == "__main__":
    unittest.main()
