import unittest
from pathlib import Path

import pandas as pd
from dateutil.parser import parse

from functions.ndvi import calculate_and_save_ndvi
from playground.average_ndvi import (
    calculate_index_stats,
    build_diagram,
    plot_index_stats,
)


class IndexCalculationTest(unittest.TestCase):
    def test_calculate_and_plot_ndvi(self):
        current_file_path = Path(__file__).parent.resolve()
        base_path = str(
            current_file_path.joinpath("./data/60m_mosaic/2021/6").resolve()
        )

        calculate_and_save_ndvi(base_path=base_path, year=2021, resolution=60, month=6)

    def test_index_stats(self):
        current_file_path = Path(__file__).parent.resolve()
        ndvi_path = current_file_path.joinpath("./data/60m_mosaic").resolve()
        config_value = {"date": parse("2021-6-1")}

        some_stats = calculate_index_stats(
            base_path=ndvi_path,
            config_value=config_value,
            filename="mosaic_R60m_NDVI.tif",
        )

        self.assertEquals(
            set(some_stats.keys()),
            set(
                {
                    "area_dense_vegetation": 208230,
                    "area_sparse_vegetation": 129002,
                    "area_no_vegetation": 21322,
                    "area_total": 123,
                    "mean_ndvi": 0.6526903,
                }.keys()
            ),
        )

    def test_diagram_stats(self):
        current_file_path = Path(__file__).parent.resolve()
        ndvi_path = current_file_path.joinpath("./data/60m_mosaic/2021/6/").resolve()
        some_paths = {
            "2020-04": {
                "ndvi_path": ndvi_path,
                "filename": "mosaic_R60m_NDVI.tif",
                "date": parse("2021-4-1"),
            },
            "2020-05": {
                "ndvi_path": ndvi_path,
                "filename": "mosaic_R60m_NDVI.tif",
                "date": parse("2021-5-1"),
            },
            "2020-06": {
                "ndvi_path": ndvi_path,
                "filename": "mosaic_R60m_NDVI.tif",
                "date": parse("2021-6-1"),
            },
        }
        build_diagram(
            some_paths, base_path=current_file_path.joinpath("./data/60m_mosaic/")
        )

    def test_plot_index_stats(self):
        current_file_path = Path(__file__).parent.resolve()
        base_path = current_file_path.joinpath("./data/60m_mosaic").resolve()

        df_stats = pd.DataFrame(
            {
                "month": {"0": "2021-01", "1": "2021-02", "2": "2021-03"},
                "area_dense_vegetation": {"0": 700, "1": 900, "2": 1356},
                "area_sparse_vegetation": {"0": 48, "1": 48, "2": 48},
                "area_no_vegetation": {"0": 3, "1": 3, "2": 3},
                "area_total": {"0": 1407, "1": 1407, "2": 1407},
                "mean_ndvi": {"0": 0.6514400125, "1": 0.7514400125, "2": 0.8514400125},
                "mean_ndsi": {"0": 0.9514400125, "1": 0.8514400125, "2": 0.7514400125},
                "dense_vegetation_ratio": {
                    "0": 0.9637526652,
                    "1": 0.9637526652,
                    "2": 0.9637526652,
                },
            }
        )
        df_stats.set_index("month", inplace=True)

        plot_index_stats(df_stats, base_path)


if __name__ == "__main__":
    unittest.main()
