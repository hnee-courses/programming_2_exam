import os
from pathlib import Path

import pandas as pd
import rasterio
from matplotlib import pyplot as plt, pyplot
import numpy as np

from functions.get_pipeline_config import load_yaml_from_disk


def dumpster_function():

    # ep.plot_bands(numpyarray,
    #               cmap='PiYG',
    #               scale=False,
    #               vmin=-1, vmax=1,
    #               title="NAIP Derived NDVI\n 19 September 2015 - Cold Springs Fire, Colorado")
    # plt.show()

    # ep.hist(numpyarray, hist_range=[0,1], title="NDVI Histogram")
    # plt.show()
    #
    # # Define a new figure
    # fig2 = plt.figure(figsize=(20, 10))
    #
    # # Give this new figure a subplot, which will contain the histogram itself
    # ax = fig2.add_subplot(111)
    #
    # # Add a title & (x,y) labels to the plot
    # plt.title("NDVI Histogram", fontsize=18, fontweight='bold')
    # plt.xlabel("NDVI values", fontsize=14)
    # plt.ylabel("Number of pixels", fontsize=14)
    #
    # # For the x-axis, we want to count every pixel that is not an empty value
    # x = numpyarray[~np.isnan(numpyarray)]
    # color = 'g'
    # # call 'hist` with our x-axis, bins, and color details
    # ax.hist(x, bins=30, color=color, histtype='bar', ec='black')

    # Save the generated figure to an external image file
    # fig2.savefig("ndvi-histogram.png", dpi=200, bbox_inches='tight', pad_inches=0.5)

    # numpyarray[ ( ( numpyarray > 0.2 ) & (numpyarray < 0.59) ) ].reshape(numpyarray.shape)
    pass


def calculate_index_stats(base_path, config_value, filename="mosaic_R60m_NDVI.tif"):
    """
    calculates statistics on an index in the range [-1...1]

    :param path:
    :return:
    """
    base_path = Path(base_path)
    path = (
        base_path.joinpath(str(config_value["date"].year))
        .joinpath(str(config_value["date"].month))
        .joinpath(filename)
    )

    with rasterio.open(str(path)) as naip_ndvi:
        numpyarray = naip_ndvi.read(1)
        average_ndvi = np.nanmean(numpyarray)

        # Define a new figure
        fig3 = plt.figure(figsize=(20, 10))

        # Give this new figure a subplot, which will contain the histogram itself
        ax = fig3.add_subplot(111)

        # Add a title & (x,y) labels to the plot
        plt.title("NDVI Histogram", fontsize=18, fontweight="bold")
        plt.xlabel("NDVI values", fontsize=14)
        plt.ylabel("Number of pixels", fontsize=14)

        mask = (numpyarray > 0.7) & (numpyarray < np.inf)
        numpyarray[np.array(mask)] = 1  # dense vegetation
        n_pixel_dense = len(numpyarray[mask])

        mask = (numpyarray > 0.4) & (numpyarray <= 0.7)
        numpyarray[np.array(mask)] = 2  # not so dense vegetation
        n_pixel_not_dense = len(numpyarray[mask])

        mask = (numpyarray > -np.inf) & (numpyarray <= 0.4)
        numpyarray[np.array(mask)] = 3  # no vegetation
        n_pixel_no_veg = len(numpyarray[mask])

        area_total = len(numpyarray[~np.isnan(numpyarray)])

        some_stats = {
            "area_dense_vegetation": n_pixel_dense,
            "area_sparse_vegetation": n_pixel_not_dense,
            "area_no_vegetation": n_pixel_no_veg,
            "area_total": area_total,
            "mean_ndvi": average_ndvi,
        }

        x = numpyarray[~np.isnan(numpyarray)]
        color = "g"
        # call 'hist` with our x-axis, bins, and color details
        ax.hist(
            x, bins=3, color=color, histtype="bar", ec="black", label="surface type"
        )
        ax.legend()
        # Save the generated figure to an external image file
        fig3.savefig(base_path.joinpath("ndvi-histogram.png"))

        pyplot.imshow(numpyarray, cmap="pink")
        # pyplot.show()
        plt.close(fig3)

        return some_stats


def build_diagram(some_paths, base_path):
    """

    :param some_paths:
    :param ndvi_path:
    :return:
    """
    import pandas as pd
    from matplotlib import dates as mdates

    statistics_list = []
    for key, value in some_paths.items():
        result_dict = calculate_index_stats(base_path=base_path, config_value=value)
        result_dict["month"] = key
        statistics_list.append(result_dict)

    df_stats = pd.DataFrame(statistics_list)
    df_stats["dense_vegetation_ratio"] = (
        df_stats["area_dense_vegetation"] / df_stats["area_total"]
    )

    return df_stats


def plot_index_stats(df_stats: pd.DataFrame, base_path: Path):
    """
    draw a diagram
    :param base_path:
    :param df_stats:
    :return:
    """
    # ---------------------------------------------------------------------
    # Part 1: Create a figure and axis using plt.subplots
    fig, ax = plt.subplots(figsize=(9, 6))
    # ax_right = ax.twinx()
    # ---------------------------------------------------------------------

    # Part 2: Plot, modify, plot again, include text, etc.
    ax.plot(
        df_stats.index, df_stats["mean_ndvi"], color="green", label="monthly mean NDVI"
    )
    ax.fill_between(df_stats.index, -1, df_stats["mean_ndvi"], color="green", alpha=0.3)

    # ax.plot(df_stats.index[0], [0], color='blue',
    #        label='Mean NDSI')  # Trick: doesn't print anything, only used to get the legend

    # ax_right.plot(df_stats.index, df_stats['mean_ndsi'], color='blue', label='Mean NDSI')

    # Format axes
    # ax.xaxis.set_major_locator(mdates.MonthLocator(bymonth=range(0, 24, 1)))
    # ax.xaxis.set_major_formatter(mdates.MonthFormatter('%H:%M'))

    ax.set_xlabel("Time", fontsize=14)
    ax.set_ylabel("NDVI", fontsize=14)
    # ax_right.set_ylabel('NDSI $[\%]$', fontsize=14)

    ax.set_ylim([-1, 1])
    # ax_right.set_ylim([-1, 1])

    # ax_right.grid(True)

    ax.legend(loc="center left")
    # ax_right.legend( loc='top right' )

    fig.suptitle("NDVI over AOI", fontsize=18)

    # ---------------------------------------------------------------------
    # Part 3: Show, save to file, and close
    fig.tight_layout(rect=[0, 0, 1, 0.99])
    # plt.show()

    # ---------------------------------------------------------------------

    image_path = base_path.joinpath("index_timeseries.png")
    plt.savefig(image_path)
    plt.close()
    return image_path
