"""
demonstation how to merge two ndvi float32 images
This is necessary https://www.researchgate.net/post/What_is_the_best_way_of_moscaicing_overlapping_images

With a VRT this works:
https://gis.stackexchange.com/questions/372827/gdal-vrt-pixel-function-no-output-with-signed-or-float-data-type

https://gis.stackexchange.com/questions/324602/averaging-overlapping-rasters-with-gdal
export GDAL_VRT_ENABLE_PYTHON=YES
gdal_translate -of GTiff temp.vrt bla.tif
gdal_translate --config GDAL_VRT_ENABLE_PYTHON YES temp.vrt bla.tif

If you get an error message check the indentation
## https://github.com/aerospaceresearch/DirectDemod/blob/Vladyslav_dev/directdemod/merger.py
"""
from pathlib import Path

import rasterio
from osgeo import gdal

test_images = [
    str(
        Path(
            "/media/christian/2TB/HNEE/GIS/tmp/mosaic/33UVV/202103/60/mosaic_R60m_NDVI.tif"
        )
    ),
    str(
        Path(
            "/media/christian/2TB/HNEE/GIS/tmp/mosaic/33UUV/202103/60/mosaic_R60m_NDVI.tif"
        )
    ),
]
out_fp = "/tmp/Schorfheide_Mosaic_local.tif"
# spatial_merge_vrt(clipped_images=test_images, out_fp=out_fp)


vrt_path = "temp.vrt"  # path to vrt to build
gdal.BuildVRT(vrt_path, test_images)
with rasterio.open(vrt_path) as raster:
    pass  # do stuff
