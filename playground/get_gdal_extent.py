from osgeo import gdal
from osgeo.gdalconst import GA_ReadOnly

def find_extent_gdal(path):
    """
    @deprecated

    calculate the extent of a georeference image.
    :param path:
    :return:
    """

    data = gdal.Open(
        "/media/christian/2TB/HNEE/GIS/fels/33UUU/S2A_MSIL2A_20210702T101031_N0301_R022_T33UUU_20210702T121353.SAFE/GRANULE/L2A_T33UUU_A031479_20210702T101157/IMG_DATA/R10m/T33UUU_20210702T101031_B02_10m.jp2",
        GA_ReadOnly,
    )
    geoTransform = data.GetGeoTransform()
    minx = geoTransform[0]
    maxy = geoTransform[3]
    maxx = minx + geoTransform[1] * data.RasterXSize
    miny = maxy + geoTransform[5] * data.RasterYSize
    print([minx, miny, maxx, maxy])
    data = None
