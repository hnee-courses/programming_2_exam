import time
import calendar
from pathlib import Path
from loguru import logger
import json

import os
from dagster import (
    monthly_partitioned_config,
)
from dagster import DynamicOut, DynamicOutput, Field, job, op
from dagster import get_dagster_logger, job, op, repository, schedule
from datetime import datetime

from fels import convert_wkt_to_scene
from fels.sentinel2 import ensure_sentinel2_metadata
from functions.get_extent import find_extent
from functions.get_pipeline_config import load_yaml_from_disk
from functions.image_merge import clip_image, spatial_merge
from functions.index_operation import build_custom_index
from functions.mosaicing import prepare_filelist_for_mosaicing, trigger_mosaicing
from functions.ndvi import calculate_and_save_ndvi


@op
def delete_s2_index(context) -> bool:
    """
    if the index is too old is has to be deleted and a new version has to be downloaded
    :return:
    """
    # app_config = get_config()
    # Load the configuration from an environment variable. Make sure you have set it before.
    YML_CONFIG_PATH = os.getenv("YML_CONFIG_PATH")
    app_config = load_yaml_from_disk(YML_CONFIG_PATH)
    index_file = Path(app_config.get("zipped_index_path")).joinpath(
        app_config.get("zipped_index_filename")
    )

    try:
        st = os.stat(index_file)
        mtime = st.st_mtime
        curr_time = time.time()
        status = False
        if curr_time - mtime > app_config.get("max_index_age"):
            context.log.info("index too old. will delete it.")

            os.remove(index_file)
            status = True

        return status
    except:
        return False


@op
def get_simple_tilelist(context):
    """
    calculates the WGRS Tiles from the passed geojson
    :param context:
    :param app_config:
    :return:
    """

    app_config = load_yaml_from_disk(os.getenv("YML_CONFIG_PATH"))

    aoi_path = app_config["aoi_path"]
    scenes = []
    current_file_path = Path(__file__).parent.resolve()

    with open(current_file_path.joinpath(aoi_path)) as f:
        geojson_data = json.loads(f.read())
        geojson_data = geojson_data.get("features")[0].get("geometry")

        scenes.extend(convert_wkt_to_scene("S2", geojson_data, True))
    scenes = list(set(scenes))

    return scenes


@op(tags={"download_s2_index": "None"})
def download_s2_index(context, status: bool, scenes: list) -> dict:
    """
    download the index containing all Level 2 Sentinel 2 processing

    On first run this downloads the csv.gz file, unzips it and converts it to a parquet file

    :param context:
    :param status:
    :return:
    """
    app_config = load_yaml_from_disk(os.getenv("YML_CONFIG_PATH"))

    sentinel2_metadata_file = ensure_sentinel2_metadata(
        app_config.get("zipped_index_path")
    )
    app_config["sentinel2_metadata_file"] = sentinel2_metadata_file

    build_custom_index(
        sentinel2_metadata_file, app_config.get("reduced_index_path"), scenes
    )
    ## FIXME if this index is built and deleted in every run you will get problems when concurrency is > 1
    return app_config


@op(
    out=DynamicOut(str),
)
def find_wgrs_tiles_for_aoi(context, app_config: dict):
    """
    calculates the WGRS Tiles from the passed geojson
    :param context:
    :param app_config:
    :return:
    """

    # app_config = load_yaml_from_disk(YML_CONFIG_PATH)
    aoi_path = app_config["aoi_path"]
    scenes = []

    with open(aoi_path) as f:
        geojson_data = json.loads(f.read())
        geojson_data = geojson_data.get("features")[0].get("geometry")

        scenes.extend(convert_wkt_to_scene("S2", geojson_data, True))
    scenes = list(set(scenes))

    for scene in scenes:
        yield DynamicOutput(
            value=scene,
            mapping_key=scene,
        )


@op(tags={"download_wgrs_tile": "wgt"})
def download_wgrs_tile(context, wgrs_tilename: str):
    """
    download the wgrs tile from google cloud storage

    :param context:
    :param wgrs_tilename:
    :return:
    """
    from fels import run_fels
    from datetime import date
    from dateutil.parser import parse

    app_config = load_yaml_from_disk(os.getenv("YML_CONFIG_PATH", "KLM"))

    thedate = context.op_config["thedate"]
    dt = parse(thedate)
    year = dt.year
    month = dt.month
    date_object = date(year, month, 1)
    res = calendar.monthrange(date_object.year, date_object.month)[1]
    bands = app_config["bands"]
    resolution = app_config["resolution"]

    urls_df = run_fels(
        None,
        "S2",  # or L1 or L8
        date(year, month, 1),
        date(year, month, res),
        cloudcover=app_config["max_cloud_cover"],
        output=app_config["file_input_folder"],
        scene=wgrs_tilename,
        latest=False,
        list=False,
        outputcatalogs=app_config.get("reduced_index_path"),
        overwrite=False,
        includeoverlap=True,
        bands=bands,
        resolution=resolution
    )
    dlogger = get_dagster_logger()

    if len(urls_df) == 0:
        dlogger.error("no captures found for search criteria.")

    metadata = {
        "wgrs_tilename": wgrs_tilename,
        "year": year,
        "month": month,
        "urls_df": urls_df,
    }
    return metadata


@op
def build_temporal_mosaic(context, data):
    """
    generate a monthly (if possible cloudfree) mosaic

    :param context:
    :param data:
    :return:
    """
    wgrs_tilename = data.get("wgrs_tilename")
    year = data.get("year")
    month = data.get("month")
    app_config = load_yaml_from_disk(os.getenv("YML_CONFIG_PATH"))

    crs = app_config["crs"]
    resolution = app_config["resolution"]
    file_input_folder = app_config["file_input_folder"]
    cc_limit = app_config["max_cloud_cover"]

    safe_file_list = prepare_filelist_for_mosaicing(
        wgrs_tilename,
        year=year,
        month=month,
        cc_limit=cc_limit,
        file_input_folder=file_input_folder,
        index_input_folder=app_config.get("reduced_index_path"),
    )
    output_path = (
        Path(app_config["mosaic_base_path"])
        .joinpath(wgrs_tilename)
        .joinpath(f"{year}_{month}")
        .joinpath(str(resolution))
    )
    output_path.mkdir(parents=True, exist_ok=True)
    if len(safe_file_list) > 0:
        extent = find_extent(safe_file_list, crs)
        output_path = trigger_mosaicing(
            wgrs_tilename,
            year,
            month,
            crs=crs,
            bands=app_config["bands"],
            resolution=resolution,
            extent=extent,
            input_folder=list(safe_file_list["images_full_path"]),
            output_path=output_path,
        )
        context.log.info(str(output_path))

        data["output_path"] = str(output_path)
        data["crs"] = str(crs)
        data["resolution"] = str(resolution)
        return data
    logger = get_dagster_logger()
    logger.error(
        f"no data available. file_list has length: {len(safe_file_list)} Maybe there are too many clouds?"
    )


@op
def spatial_merge_op(context, data_list):
    """
    merge all tiles in the are of interest and clip them

    :param data_list:
    :param context:
    :param image_paths:
    :return:
    """
    app_config = load_yaml_from_disk(os.getenv("YML_CONFIG_PATH"))
    context.log.info(f"passed data_list to the operator: {data_list}")

    data_list = list(filter(None.__ne__, data_list))
    if len(data_list) == 0:
        raise ValueError("no data to merge")

    out_fp = app_config["mosaic_base_path"]
    aoi_path = app_config["aoi_path"]
    resolution = app_config["resolution"]

    image_paths = [imagepath["output_path"] for imagepath in data_list]
    # This hack is necessary to save the spatial mosaics in a new folder structure
    month = list(set([x["month"] for x in data_list]))[
        0
    ]  # it is guaranteed we only have a single month in the data
    year = list(set([x["year"] for x in data_list]))[
        0
    ]  # it is guaranteed we only have a single month in the data
    out_fp = Path(out_fp).joinpath(str(year)).joinpath(str(month))
    out_fp.mkdir(parents=True, exist_ok=True)

    band_mosaics = app_config["bands"]
    merged_images = spatial_merge(
        images_base=image_paths,
        band_mosaics=band_mosaics,
        resolution=resolution,
        mosaic_base_path=out_fp,
    )

    for clipped_image in merged_images:
        out_tif = str(
            Path(clipped_image).parent.joinpath(f"{clipped_image}_clipped.tif")
        )
        clip_image(
            input_path=clipped_image,
            aoi_path=aoi_path,
            resolution=resolution,
            output_path=out_tif,
        )

    return {"out_fp": out_fp, "year": year, "month": month, "resolution": resolution}


@op
def calculate_ndvi_on_mosaic(context, input_config: dict):
    """
    calculate the ndvi from a folder
    :param context:
    :param output_path:
    :return:
    """
    input_path = input_config["out_fp"]
    year = input_config["year"]
    month = input_config["month"]
    resolution = input_config["resolution"]
    ndvi_path = calculate_and_save_ndvi(
        input_path, year=year, month=month, resolution=resolution
    )

    return ndvi_path


@monthly_partitioned_config(start_date=datetime(2019, 1, 1))
def my_partitioned_config(start: datetime, _end: datetime):
    return {
        "ops": {
            "download_wgrs_tile": {
                "config": {"thedate": start.strftime("%Y-%m-%d")},
            }
        },
        "execution": {"config": {"multiprocess": {"max_concurrent": 7}}},
    }


@job(config=my_partitioned_config)
def ndvi_gif_calculation():

    wgrs_tiles = find_wgrs_tiles_for_aoi(
        download_s2_index(delete_s2_index(), get_simple_tilelist())
    )
    metadata = wgrs_tiles.map(download_wgrs_tile)
    temporal_mosaics = metadata.map(build_temporal_mosaic).collect()

    clipped_raster = spatial_merge_op(temporal_mosaics)
    result = calculate_ndvi_on_mosaic(clipped_raster)


@schedule(
    cron_schedule="0 */24 * * *",
    job=ndvi_gif_calculation,
    execution_timezone="Europe/Berlin",
)
def good_morning_schedule(context):
    date = context.scheduled_execution_time.strftime("%Y-%m-%d")
    return {"ops": {"download_wgrs_tile": {"config": {"thedate": date}}}}


@repository
def satellite_imagery_repository():
    return [ndvi_gif_calculation, good_morning_schedule]


if __name__ == "__main__":
    """
    simple demo case to download two months of data
    """
    # for i in range(1,13):

    ## use a path which has at least 40GB empty
    base_path = "/media/christian/2TB/tmp"
    ## use a path which has at least 40GB empty

    current_file_path = Path(__file__).parent.resolve()
    yml_path = current_file_path.joinpath(
        "./configuration/pipeline_configuration_demo.yml"
    ).resolve()

    os.environ["YML_CONFIG_PATH"] = str(yml_path)

    thedate = f"2021-{11}-15"
    result = ndvi_gif_calculation.execute_in_process(
        {
            "ops": {
                "download_wgrs_tile": {"config": {"thedate": thedate}},
            },
            "execution": {"config": {"max_concurrent": 4}},
        }
    )
