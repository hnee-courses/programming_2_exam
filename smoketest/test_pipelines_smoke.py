import os
import unittest
from pathlib import Path

from functions.get_pipeline_config import _build_config_yml
from main import pipeleine_all


class SmokeTest(unittest.TestCase):
    def test_smoketest(self):
        """
        a test for everything. If this runs the program runs
        :return:
        """
        current_file_path = Path(__file__).parent.resolve()

        yml_path = current_file_path.joinpath(
            "../configuration/pipeline_configuration_demo.yml"
        ).resolve()
        # aoi_path = current_file_path.joinpath(
        #     "./configuration/area_of_interest/storm_damage_eberswalde_forest_campus.geojson").resolve()

        # _build_config_yml(data_base_path=current_file_path.joinpath("/tmp/"),
        _build_config_yml(
            data_base_path=Path("/tmp/"),
            aoi_path=current_file_path.joinpath(
                "../configuration/area_of_interest/storm_damage_eberswalde_forest_campus.geojson"
            ).resolve(),
            crs=32633,
            yaml_path=yml_path,
        )

        os.environ["YML_CONFIG_PATH"] = str(yml_path)

        pipeleine_all()


if __name__ == "__main__":
    unittest.main()
