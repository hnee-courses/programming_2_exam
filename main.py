import os
from pathlib import Path

from dateutil.relativedelta import relativedelta

from functions.get_pipeline_config import _build_config_yml, load_yaml_from_disk
from satellite_first_stage_pipeline import ndvi_gif_calculation
from satellite_second_stage_pipeline import timeseries_analytics_job
from loguru import logger


def pipeleine_all():
    """
    execute everything which will give you some nice images. But with due do missing parallelization this will take a while

    :return:
    """
    app_config = load_yaml_from_disk(os.getenv("YML_CONFIG_PATH"))
    from dateutil.parser import parse

    start_date = parse(app_config["start_date"])
    end_date = parse(app_config["end_date"])
    resolution = app_config["resolution"]
    band = app_config["band"]

    date_after_month = start_date
    months = []
    i = 1
    while date_after_month < end_date:
        months.append(date_after_month)
        date_after_month = start_date + relativedelta(months=i)
        i += 1

    for thedate in months:  # = f"2020-{4}-15"
        thedate = thedate.strftime("%Y-%m-%d")
        ## the process has the monthly interval hardcoded
        result = ndvi_gif_calculation.execute_in_process(
            {
                "ops": {
                    "download_wgrs_tile": {"config": {"thedate": thedate}},
                },
            }
        )
        logger.info(f"finished processing timeseries for {thedate}")

    result = timeseries_analytics_job.execute_in_process({})

    logger.info(f"finished processing timeseries.")


if __name__ == "__main__":
    # current_file_path = Path(__file__).parent.resolve()
    #
    #
    #
    # current_file_path = Path(__file__).parent.resolve()
    # yml_path = current_file_path.joinpath("./configuration/pipeline_configuration_demo.yml").resolve()
    # # aoi_path = current_file_path.joinpath(
    # #     "./configuration/area_of_interest/storm_damage_eberswalde_forest_campus.geojson").resolve()
    #
    # _build_config_yml(data_base_path=base_path, aoi_path=aoi_path,
    #                                  yaml_path=yml_path, crs=crs)
    #
    # os.environ["YML_CONFIG_PATH"] = str(yml_path)
    #
    # pipeleine_all(base_path=current_file_path.joinpath("./tests/data/"))
    pass
