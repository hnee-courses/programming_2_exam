"""
process images into a small gif animation

"""
import os
from pathlib import Path
from dagster import get_dagster_logger, job, op, repository, schedule
from dateutil.parser import parse
from loguru import logger

from functions.get_pipeline_config import (
    load_yaml_from_disk,
    build_config_yml,
    _build_config_yml,
)
from playground.average_ndvi import (
    calculate_index_stats,
    build_diagram,
    plot_index_stats,
)


@op
def generate_filelist_conf(context):
    from dateutil.relativedelta import relativedelta

    YML_CONFIG_PATH = os.getenv("YML_CONFIG_PATH")
    app_config = load_yaml_from_disk(YML_CONFIG_PATH)

    start_date = parse(app_config["start_date"])
    end_date = parse(app_config["end_date"])
    resolution = app_config["resolution"]
    band = app_config["band"]

    date_after_month = start_date
    months = []
    i = 1
    while date_after_month < end_date:
        months.append(date_after_month)
        date_after_month = start_date + relativedelta(months=i)
        i += 1

    animation_configs = {}
    for datum in months:
        year = str(datum.year)
        month = str(datum.month)

        animation_config = {
            "filepath": Path(app_config["mosaic_base_path"])
            .joinpath(year)
            .joinpath(month)
            .joinpath(f"{band}_R{resolution}m.png"),
            "date": datum,
            "band": band,
        }
        animation_configs[f"{year}_{month}"] = animation_config

    dlogger = get_dagster_logger()
    dlogger.info(f"produced this configuration: {animation_configs}")

    return animation_configs


@op
def build_animation(context, animation_configs: dict):
    """
    build a gif animation from the single maps defined in animation_configs

    :param context:
    :param animation_configs:
    :return:
    """
    import imageio as io

    YML_CONFIG_PATH = os.getenv("YML_CONFIG_PATH")
    app_config = load_yaml_from_disk(YML_CONFIG_PATH)

    animation_path = app_config["animation_path"]
    band = app_config["band"]
    start_date = app_config["start_date"]
    end_date = app_config["end_date"]

    output_path = Path(animation_path).joinpath(
        f"animation_{start_date}_to_{end_date}_{band}.gif"
    )
    with io.get_writer(output_path, mode="I", duration=1.5) as writer:

        for key, config_elem in animation_configs.items():
            try:
                image = io.imread(config_elem["filepath"])
                writer.append_data(image)
            except FileNotFoundError as e:
                print(e)
                logger.error(f"File not found: {e}")

    writer.close()
    return output_path


@op
def statistics_on_gtiff(context, animation_configs: list):
    """
    build a gif animation from the single maps defined in animation_configs

    :param context:
    :param animation_configs:
    :return:
    """
    YML_CONFIG_PATH = os.getenv("YML_CONFIG_PATH")
    app_config = load_yaml_from_disk(YML_CONFIG_PATH)

    for config_elem in animation_configs:
        try:
            stats_df = calculate_index_stats(
                base_path=app_config["animation_path"],
                config_value=config_elem,
                filename="mosaic_R60m_NDVI.tif",
            )
        except FileNotFoundError as e:
            print(e)
            logger.error(f"File not found: {e}")

    return stats_df


@op
def index_statistics(context, filelist):
    app_config = load_yaml_from_disk(os.environ["YML_CONFIG_PATH"])
    base_path = app_config["animation_path"]

    df_stats = build_diagram(filelist, base_path=base_path)
    plot_index_stats(df_stats, base_path=Path(base_path))
    return df_stats


@job
def timeseries_analytics_job():

    filelist = generate_filelist_conf()

    # ndvi_path = current_file_path.joinpath("./data/60m_mosaic").resolve()

    animation_output_path = build_animation(filelist)

    ds_stats = index_statistics(filelist)

    dlogger = get_dagster_logger()
    dlogger.info(f"wrote animation to: {animation_output_path}")



if __name__ == "__main__":
    """
    simple demo case to process a timeseries of satellite
    """

    base_path = "/media/christian/2TB/tmp"

    current_file_path = Path(__file__).parent.resolve()
    yml_path = current_file_path.joinpath(
        "./configuration/pipeline_configuration_demo.yml"
    ).resolve()
    # aoi_path = current_file_path.joinpath(
    #     "./configuration/area_of_interest/storm_damage_eberswalde_forest_campus.geojson").resolve()
    #
    # _build_config_yml(data_base_path=base_path, aoi_path=aoi_path,
    #                  yaml_path=yml_path, crs="")
    os.environ["YML_CONFIG_PATH"] = str(yml_path)

    # thedate = "2021-12-04"
    result = timeseries_analytics_job.execute_in_process(
        {
            "ops": {
                # "generate_gif_from_png": {"config": {"thedate": thedate}},
            },
            # "execution": {"config": {"max_concurrent": 1}},
        }
    )
