
\section{Materials and Methods}
Since none of the evaluated tools where sufficient or too complicated for a proof of concept the following should show how all calculations can be done with no cloud computing using open source tools.

\subsection{Software Libraries}
The core libraries for the implementation are 
\begin{itemize}
  \item \href{https://dagster.io/}{Dagster}
  \item \href{https://github.com/cwinkelmann/fetchLandsatSentinelFromGoogleCloud}{\ac{FeLS}}
  \item \href{https://spark.apache.org/docs/latest/api/python/}{PySpark} - parallize sentinel index processing
    \item \href{https://parquet.apache.org/}{parquet} - columnar data format.
    \item \href{https://sen2mosaic.readthedocs.io/}{Sen2Mosaic} to get cloud free wgrs tiles for a month.

\end{itemize}

\subsection{Data Retrieval}
Sentinel-2 imagery was obtained using the google cloud storage bucket ( \href{https://cloud.google.com/storage/docs/public-datasets/sentinel-2}{GCP}). Three resolutions of 10m, 20m and 60m per pixel are provided. For each resolution different bands are available \parencite{S2_band_description}. To reduce preprocessing and therefore avoid using FORCE/MAJA/SNAP Level 2 A was selected. Google Cloud provides two ways of searching for specific tiles. There is an index file and a BigQuery endpoint. To simplify implementation a library for searching and downloading was selected. The library  ( \ac{FeLS} \parencite{Nunes2021} has a straightforward way of passing an \ac{AOI} as a geojson, find all \ac{WGRS} tiles which overlap (See Figure \ref{fig:wgrs_tiling} and download. While using v1.4 and the master branch performance and functional problems occurred. A \href{https://github.com/cwinkelmann/fetchLandsatSentinelFromGoogleCloud}{fork} was created, which reduced the time of the initial index setup by using Spark for parallelization and Parquet as an intermediate data format. Additionally it allows limiting the download to specific resolutions and bands.
The \ac{AOI} was drawn and downloaded using \href{https://geojson.io/}{geojson.io}

\begin{figure}[H]
\centering
\includegraphics[width=12cm]{figures/Screenshot at 2022-01-02 20-44-15.png}
\caption{ WGRS Tiling used by sentinel 2}
\label{fig:wgrs_tiling}
\end{figure}

Listing \ref{lst:spark_parquet} shows how the zipped index file from sentinel is read with a schema on read operation, repartitioned by MGRS\_TILE and written to disk again. 



\begin{lstlisting}[language=Python, label={lst:spark_parquet}, caption=converting the gzipped index file to 5 parquet files]
schema = StructType() \
    .add("GRANULE_ID", StringType(), True) \
    .add("PRODUCT_ID", StringType(), True) \
    .add("DATATAKE_IDENTIFIER", StringType(), True) \
    .add("MGRS_TILE", StringType(), True) \
    .add("SENSING_TIME", TimestampType(), True) \
    .add("TOTAL_SIZE", IntegerType(), True) \
    .add("CLOUD_COVER", DoubleType(), True) \
    .add("GEOMETRIC_QUALITY_FLAG", BooleanType(), True) \
    .add("GENERATION_TIME", DateType(), True) \
    .add("NORTH_LAT", DoubleType(), True) \
    .add("SOUTH_LAT", DoubleType(), True) \
    .add("WEST_LON", DoubleType(), True) \
    .add("EAST_LON", DoubleType(), True) \
    .add("BASE_URL", StringType(), True) \

df = spark.read.format("csv").option("header", True)
    .schema(schema).load(zipped_index_path)

df.repartitionByRange(5, ["MGRS_TILE"]).write.mode('overwrite').parquet(index_path)
\end{lstlisting}

To speed this up even further a multistage index built was preferred in which the zipped index is unzipped, then the csvs are split, converted to parquet and then later filtered for only the wgrs tiles which are needed. The schematic code is shown in Listing \ref{lst:spark_parquet_opt}. Listing \ref{lst:spark_read_parquet} shows how to read the reduced index. Since Parquet has the schema embedded already it is not necessary to define it again. Converting it to a pandas dataframes allows to use the data further.

\begin{lstlisting}[language=Python, label={lst:spark_parquet_opt}, caption=converting index file to filtered parquet file]
# copy gzip content into folder
with gzip.open(zipped_index_path) as gzip_index, open(index_path_csv, 'wb') as f:
    shutil.copyfileobj(gzip_index, f)
# split csv in parts of lenght of 3500000 rows.
os.system(f"split -d -l 3500000 {index_path_csv} {index_path_csv_chunks}/index_part_")
    
# convert files to parquet
spark_df = spark.read.format("csv").option("header", False).schema(schema).load(index_path_csv_chunks)
spark_df.write.mode('overwrite').parquet(index_path_parquet)

...

query = (f"select * from ParquetTable where MGRS_TILE IN ({scenes_str})")
tilelist_df = spark.sql(query)
tilelist_df.repartition(1).write.mode('overwrite').parquet(reduced_index_path)
## output is < 1MB in size and contains only wgrs tiles needed later in the processing.

\end{lstlisting}





\begin{lstlisting}[language=Python, label={lst:spark_read_parquet},  caption=Query an Index File using PySpark]

parqDF = spark.read.parquet(collection_file)
parqDF.createOrReplaceTempView(f"ParquetTable_{tile}")
query = (f"select * from ParquetTable_{tile} where MGRS_TILE IN ('{tile}') AND SENSING_TIME >= '{date_start}' AND SENSING_TIME <= '{date_end}'")
if cc_limit:
    query += f" AND CLOUD_COVER <= {cc_limit}"
tilelist_df = spark.sql(query).toPandas()
spark.stop()
return tilelist_df

\end{lstlisting}


\subsection{Mosaicing the Data}
Every month of each WGRS Tile was mosaiced. The library sen2mosaic \parencite{bowers2020} is used to generate a cloud free image if there was at least one image with a cloud free pixel. Due to problems in the implementation and for debugging purposes the cli was triggered from within python. For performance optimisation the  \href{https://github.com/cwinkelmann/sen2mosaic}{project was forked} to enable a filter for specific bands. 


\subsection{Chaining the Single Operations}
To chain all operations together an \ac{DAG} is defined. Compared to just chaining the functions together this allows parellization up to scaling out in the background by dagster. Listing \ref{lst:DAG_defintion} shows how the code looks.


\begin{lstlisting}[language=Python,  label={lst:DAG_defintion}, caption=definition of the DAG]

def ndvi_gif_calculation():

    wgrs_tiles = find_wgrs_tiles_for_aoi(
        download_s2_index(
            delete_s2_index(), get_simple_tilelist()
        ))
    metadata = wgrs_tiles.map(download_wgrs_tile)
    temporal_mosaics = metadata.map(build_temporal_mosaic).collect()

    clipped_raster = spatial_merge_op(temporal_mosaics)
    result = calculate_ndvi_on_mosaic(clipped_raster)

\end{lstlisting}






